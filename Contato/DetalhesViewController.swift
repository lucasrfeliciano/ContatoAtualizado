//
//  DetalhesViewController.swift
//  Contato
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

class DetalhesViewController: UIViewController {

    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var numero: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var endereco: UILabel!
    
    public var contato: Contato?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = contato?.nome
        
        nome.text = contato?.nome
        numero.text = contato?.telefone
        email.text = contato?.email
        endereco.text = contato?.endereco
        
        
        
    }
    

   

}
